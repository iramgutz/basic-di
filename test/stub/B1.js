'use strict'

import C1 from './C1'

export default class B1 {
  static get injectable () {
    return true
  }

  static get inject () {
    return [
      C1
    ]
  }
}
