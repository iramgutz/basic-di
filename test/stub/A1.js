'use strict'

import B1 from './B1'
import B2 from './B2'

export default class A1 {
  static get injectable () {
    return true
  }

  static get inject () {
    return [
      B1,
      B2
    ]
  }
}
