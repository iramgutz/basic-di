'use strict'

export default class NoInjectable {
  static get injectable () {
    return false
  }
}
