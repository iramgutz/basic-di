'use strict'

import test from 'ava'
import di from '../src'

import A1 from './stub/A1'
import NoInjectableClass from './stub/NoInjectable'

test('should return a instance with dependencies', t => {
  let a1 = di.create(A1)

  t.true(a1 instanceof A1)

  let noClass = {}

  let noClassInstance = di.create(noClass)

  t.deepEqual(noClassInstance, noClass)
})

test('should return a same class', t => {
  let noClass = {}

  let noClassInstance = di.create(noClass)

  t.deepEqual(noClassInstance, noClass)
})

test('should return a same object', t => {
  let NoInjectable = di.create(NoInjectableClass)

  t.deepEqual(NoInjectable, NoInjectableClass)
})
