'use strict'

/**
 *
 */
class IoC
{
  /**
   *
   * @param class
   * @returns {*}
   */

  create (_class) {
    if (!IoC.isClass(_class) || !_class.injectable) {
      return _class
    }

    var injects = _class.inject || []

    var dependencies = injects.map((dependency) => this.create(dependency))

    return new _class(...dependencies)
  }

  /**
   *
   * @param fn
   * @returns {boolean}
   */
  static isClass (fn) {
    return typeof fn === 'function' && /^(?:class\s+|function\s+(?:_class|_default|[A-Z]))/.test(fn)
  }
}

export default new IoC()
