# Basic Dependency Injection

## Installation

```sh
npm install --save basic-di
```

## Use

Create a Clas with static getter methods: **injectable** and **inject**

#### injectable

Determinate if basic di returns a class or a instance of class.

```js
static get injectable () {
    return true
}
```

#### inject

A dependencies array that are setted as class instance properties

```js
static get inject () {
    return [
      B1,
      B2
    ]
}
```

## Example

UserController.js
```js
'use strict'

import UserRepository as Repository from './UserRepository'
import UserManager as Manager from './UserManager'

export default class UserController {
  constructor (Repository, Manager) {
    this.Repository = Repository
    this.Manager = Manager
  }
  
  static get injectable () {
    return true
  }

  static get inject () {
    return [
      Repository,
      Manager
    ]
  }
}
```

Create a instance of A1 with **basic di**

index.js
```js
'use strict'

import di from 'basic-id'
import UserController from './UserController'

let userController = di.create(UserController) // Return a instance of UserController with dependencies

userController.Repository // Instance of UserRepository
userController.Manager // Instance of ManagerRepository
```